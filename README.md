
---------------------------------------------
D/d - 16 wartości
Ri/Rm - 38 wartości
Rt/Rm - 38 wartości
120 wartości na osi L/d
Struktura pliku BAZA2.BIN:
Wszystkie liczby są typu float (4byty)
120 wartości L/d (logarytmów dziesiętnych)
D/d Ri/Rm Rt/Rm
120 wartości Ra/Rm (logarytmów dziesiętnych)
D/d Ri/Rm Rt/Rm
120 wartości Ra/Rm (logarytmów dziesiętnych)
.
.
D/d Ri/Rm Rt/Rm
120 wartości Ra/Rm (logarytmów dziesiętnych)
---------------------------------------------
Dla D/d=1 krzywe nie zależą od parametru Ri/Rm
W pliku BAZA2.BIN występują wszystkie rekordy kombinacji
D/d=1 oraz Ri/Rm= ....
Przy pierwszym przyciśnięciu pozycji menu Up należy je pominąć
i przejść do pierwszego rekordu gdzie D/d > 1
Także w przypadku cofania się przy pomocy pozycji menu Down
jeśli znajdujemy się na pozycji pierwszego rekordu gdzie D/d >1 to
przycisnięcie tej pozycji powinno nas ustawić na początku pliku.
Jeśli znajdujemy się na początku pliku to pozycja Down powinna
być przyblokowana.
Podobnie jeśli znajdujemy się na końcu pliku (ostatni rekord) to
pozycja Up powinna być przyblokowana.