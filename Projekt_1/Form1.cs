﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Diagnostics;
using System.Windows.Forms.DataVisualization.Charting;
using System.Web;

using System.IO;
using System.Globalization;

namespace Projekt_1
{
    public partial class Form1 : Form
    {
        List<float> floats = new List<float>();
        List<float> RtRm_value = new List<float>();
        List<float> RiRm_value = new List<float>();
        List<float> Dd_value = new List<float>();

        int RtRm_counter = 122;
        int Dd_counter = 120;
        int RiRm_counter = 121;
        int which_index_Dd = 0;
        int which_index_RiRm = 0;
        int first_index_to_draw_plot = 123 * 38 * 36;
        int first_plot_without_parameters = 1;

        public Form1()
        {
            //wczytywanie pliku
            loadFile();

            InitializeComponent();
            chart1.Series.Clear();

            drawChart(1);
        }

        public void loadFile()
        {
            StreamReader sr = new StreamReader("dane.txt");
            int counter = 0;

            while (counter < 2841912)
            {
                float tmp = float.Parse(sr.ReadLine());
                floats.Add(tmp);

                if (counter == RtRm_counter && RtRm_value.Count()<=38)
                {
                    RtRm_value.Add(tmp);
                    RtRm_counter += 123;
                }

                if (counter == RiRm_counter && RiRm_value.Count() < 38)
                {
                    RiRm_value.Add(tmp);
                    RiRm_counter += 123*38;
                }

                if (counter == Dd_counter && Dd_value.Count() <= 38)
                {
                    Dd_value.Add(tmp);
                    Dd_counter += 123*38*38;
                }
                counter++;
            }
        }
        //if up_button 1
        public void drawChart(int up_button)
        {
            chart1.Series.Clear();

            // disabled button DOWN when is first plot
            if(which_index_Dd == 0 && which_index_RiRm == 0)
            {
                button2.Enabled = false;
            }
            else
            {
                button2.Enabled = true;

            }

            var chart = chart1.ChartAreas[0];
            chart.AxisX.IntervalType = DateTimeIntervalType.Number;

            chart.AxisX.LabelStyle.Format = "";
            chart.AxisY.LabelStyle.Format = "";
            chart.AxisY.LabelStyle.IsEndLabelVisible = true;

            chart.AxisX.Minimum = -0.50;
            chart.AxisX.Maximum = 2.00;
            chart.AxisY.Minimum = -2.00;
            chart.AxisY.Maximum = 5.00;
            chart.AxisX.Interval = 0.25;
            chart.AxisY.Interval = 0.5;

            if (up_button == 1 && first_plot_without_parameters == 1)
            {
                //ustawienie indeksow do wykresow dla kolejnych parametrow d i r
                first_index_to_draw_plot += 123 * 38;
                label1.Text = first_index_to_draw_plot.ToString();
            }

            if(up_button == 0)
            {
                //ustawienie indeksow do wykresow dla kolejnych parametrow d i r
                first_index_to_draw_plot -= 123 * 38;
                label1.Text = first_index_to_draw_plot.ToString();
            }


            int i = 0;
            int k = 0;
            for (int j = 0; j<38; j++)
            {
                // create chart
                // j do indeksowania ilosci plotow na jednym wykresie
                chart1.Series.Add(RtRm_value[j].ToString());
                chart1.Series[RtRm_value[j].ToString()].ChartType = SeriesChartType.Spline;
                //indeks k by indeksowac po x', natomiast i po y'
                for (i = first_index_to_draw_plot, k=0; i < first_index_to_draw_plot + 120; i++,k++)
                {
                    chart1.Series[RtRm_value[j].ToString()].Points.AddXY(floats[k], floats[i + 123*(j+1)]);
                }
            }
            

            Dd_label.Text = "";
            RiRm_label.Text = "";

            Dd_label.Text += "D/d = " + Dd_value[which_index_Dd];
            RiRm_label.Text += "Ri/Rm = " + RiRm_value[which_index_RiRm];

            Console.WriteLine(which_index_RiRm.ToString());

            label2.Text = which_index_Dd.ToString();
            label3.Text = which_index_RiRm.ToString();
        }

        // button UP
        private void button1_Click(object sender, EventArgs e)
        {
            if(first_plot_without_parameters == 1)
            {
                Console.WriteLine("DEBIILU\n");
            }

            RiRm_counter += 123;

            if (which_index_RiRm == RiRm_value.Count-1)
            {
                which_index_RiRm = 0;
                which_index_Dd++;
                Dd_counter += 123;
                drawChart(1);
            }
            else
            {
                ++which_index_RiRm;
                drawChart(1);
            }
        }

        // button DOWN
        private void button2_Click(object sender, EventArgs e)
        {
            if (which_index_RiRm == 0)
            {
                which_index_RiRm = RiRm_value.Count-1;
                which_index_Dd--;
                Dd_counter -= 123;
                drawChart(0);
            }
            else
            {
                RiRm_counter -= 123;
                --which_index_RiRm;
                drawChart(0);
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
